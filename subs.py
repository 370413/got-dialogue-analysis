# Author: Tomasz Necio
# Subtitles source: https://www.kaggle.com/gunnvant/game-of-thrones-srt/downloads/game-of-thrones-srt.zip/1
# Subtitles for season 8 from tvsubtitles.net (MEMENTO version)

# Parse subs for seasons 1–7 from json dataset

import json
import matplotlib.pyplot as plt
import numpy as np

def is_spoken_line(line):
    line = line.strip()
    if len(line) < 2 or line[0].isdigit() or line[0] == "<" or line[0] == "[" or line[0] == "(":
        return False
    return True

subs = {}
episode_numbers = []

for num in range(1, 8):
    file_name = "season" + str(num) + ".json"
    with open(file_name, 'r') as file:
        data = json.load(file)
        for key, episode_sub in data.items():
            try:
                episode_number = 100 * num + int(key[20:22])
                subs[episode_number] = sum([len(line.split()) for line in filter(is_spoken_line, episode_sub.values())])
                episode_numbers.append(episode_number)
            except Exception:
                print(key)

# Parse srt files with subs for season 8

for num in range(1, 7):
    file_name = "s8e" + str(num) + ".srt"
    with open(file_name, 'r') as file:
        file_content = [s.strip() for s in list(filter(is_spoken_line, file))]
        subs[800 + num] = sum([len(line.split()) for line in file_content])
        episode_numbers.append(800 + num)

# bars from example on stackoverflow.com/questions/10369681

width = 0.05     # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)
for i in range(10):
    y = [subs[en] for en in episode_numbers if en % 100 == i + 1]
    ind = np.arange(len(y))
    ax.bar(ind + width * i, y, width, color='royalblue')

ax.set_ylabel('Length (words)')
ax.set_title('Length of spoken lines by episode')
ax.set_xticks(np.arange(8) + width / 2)
ax.set_xticklabels(["S" + str(n) for n in range(1, 9)])

plt.savefig("w_len_ep")
plt.clf()

# and now, divide this by episode length (from Wikipedia)

episode_lens = [
    62, 56, 58, 56, 55, 53, 58, 59, 57, 53,
    53, 54, 53, 51, 55, 54, 56, 54, 55, 64,
    55, 57, 53, 54, 58, 53, 57, 56, 52, 62,
    58, 52, 57, 55, 53, 50, 51, 52, 50, 65,
    52, 55, 60, 50, 57, 54, 59, 60, 53, 61,
    60, 54, 53, 59, 57, 52, 51, 59, 60, 68,
    59, 59, 63, 50, 59, 70, 80,
    54, 58, 82, 78, 80, 80
]

fig = plt.figure()
ax = fig.add_subplot(111)
for i in range(10):
    y = [subs[en] / el for el, en in zip(episode_lens, episode_numbers) if en % 100 == i + 1]
    ind = np.arange(len(y))
    ax.bar(ind + width * i, y, width, color='seagreen')

ax.set_ylabel('Length (wordsper minute)')
ax.set_title('Length of spoken lines by episode divided by episode length')
ax.set_xticks(np.arange(8) + width / 2)
ax.set_xticklabels(["S" + str(n) for n in range(1, 9)])

plt.savefig("w_avg_len_ep")
plt.clf()

# totals and averages per season

def avg(l):
    return sum(l) / len(l)

avg_subs = [avg([subs[en] for en in episode_numbers if int(en / 100) == i + 1]) for i in range(8)]
plt.plot([i for i in range(1, 9)], avg_subs)
plt.title("Total length of dialogue per season")
plt.ylabel("Total length of dialogue (words)")
plt.xlabel("Season")
plt.savefig("w_len_s")
plt.clf()

avg_subs_per_len = [sum([subs[en] for el, en in zip(episode_lens, episode_numbers) if int(en / 100) == i + 1]) / sum([el for el, en in zip(episode_lens, episode_numbers) if int(en / 100) == i + 1]) for i in range(8)]
plt.plot([i for i in range(1, 9)], avg_subs_per_len)
plt.title("Length of dialogue divided by season length in minutes")
plt.ylabel("Average length of dialogue (words / minute)")
plt.xlabel("Season")
plt.savefig("w_avg_len_s")
plt.clf()
